var iotApp = angular.module('IoT', []);
iotApp.controller("main", ["$scope","$interval","$http", function ($scope, $interval, $http) {

    $scope.isConfigured = false;

    $scope.mainPageOpen = true;

    $scope.statsPageOpen = false;

    $scope.sysInfPageOpen = false;

    $scope.choosenCoffee = 'SMALL';

    $scope.coffeeCount = 0;

    $scope.updateStatus = function () {
        console.log("Updating status");
        $http({
            method: 'GET',
            url: 'http://localhost:8080/coffee/status'
        }).then(function successCallback(response) {
            var responseJSON = response.data;
            //isConfigured = responseJSON.configured;
            isConfigured = responseJSON.configured;
            $scope.updatePage(responseJSON.status,responseJSON.configured);
        }, function errorCallback(response) {
            var responseJSON = response.data;
            isConfigured = false;
        });
    }

    $scope.openMainPage =  function() {
        $scope.mainPageOpen = true;
        $scope.statsPageOpen = false;
        $scope.sysInfPageOpen = false;
    }

    $scope.openStatisticsPage = function() {
        $scope.mainPageOpen = false;
        $scope.statsPageOpen = true;
        $scope.sysInfPageOpen = false;
    }

    $scope.openInformationPage = function() {
        $scope.mainPageOpen = false;
        $scope.statsPageOpen = false;
        $scope.sysInfPageOpen = true;
    }

    $scope.chooseSmallCoffee = function () {
        $scope.choosenCoffee = 'SMALL';
        $("#smallCoffeeButton").addClass("active");
        $("#largeCoffeeButton").removeClass("active");
    }

    $scope.chooseLargeCoffee = function () {
        $scope.choosenCoffee = 'BIG';
        $("#smallCoffeeButton").removeClass("active");
        $("#largeCoffeeButton").addClass("active");
    }

    $scope.makeCoffee = function () {

        if(!isConfigured) {
            return;
        }

        $("#coffeeButton").css("display", "none");
        $("#loadingDiv").css("display", "block");

        $("#smallCoffeeButton").css("display", "none");
        $("#largeCoffeeButton").css("display", "none");

        console.log("Making coffee");
        $http({
            method: 'POST',
            url: 'http://localhost:8080/coffee/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: { coffeeType: $scope.choosenCoffee  }
        }).then(function successCallback(response) {

        }, function errorCallback(response) {
            var responseJSON = response.data;
        });

    }

    $scope.updatePage = function(status, isConfigured) {
        if(!isConfigured) {
            $("#coffeeButton").css("display", "block");
            $("#loadingDiv").css("display", "none");
            $("#coffee").removeClass("coffee").addClass("warning sign");
            $("#statusMessage").text("Cafe isn't configured");
            $("#smallCoffeeButton").css("display", "none");
            $("#largeCoffeeButton").css("display", "none");
        } else {
            if(status === "READY") {
                $("#coffeeButton").css("display", "block");
                $("#loadingDiv").css("display", "none");
                $("#coffee").removeClass("warning sign").addClass("coffee");
                $("#statusMessage").text("Click to get a coffee!");
                $("#smallCoffeeButton").css("display", "inline");
                $("#largeCoffeeButton").css("display", "inline");
            } else {
                $("#coffeeButton").css("display", "none");
                $("#loadingDiv").css("display", "block");
                $("#loadingText").text(status);
                $("#smallCoffeeButton").css("display", "none");
                $("#largeCoffeeButton").css("display", "none");
            }
        }
    }

    $interval(function() {$scope.updateStatus()},1500);
    
}]);

