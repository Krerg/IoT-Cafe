package com.iot.pojo;

/**
 * @author isudarik
 * @since 14.03.2017
 */
public enum ClientType {
    MACHINE, PLATFORM;
}
