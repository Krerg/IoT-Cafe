package com.iot.pojo.response;

/**
 * Created by amylnikov on 14.08.17.
 */
public class StatisticsResponse {

    public int coffeeCount;

    public int distance;

    public HourStat[] hourStats;

}
