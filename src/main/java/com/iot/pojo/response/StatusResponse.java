package com.iot.pojo.response;

/**
 * @author amylnikov
 */
public class StatusResponse {

    private boolean isConfigured;

    private String status;

    public boolean isConfigured() {
        return isConfigured;
    }

    public void setConfigured(boolean configured) {
        this.isConfigured = configured;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
