package com.iot.pojo.response;

/**
 * @author isudarik
 * @since 14.03.2017
 */
public class RegisterResponse {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
