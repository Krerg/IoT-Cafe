package com.iot.pojo.response;

/**
 * @author amylnikov
 */
public class SystemInformationResponse {

    public String apolloMiniIp;

    public String coffeeMachineIp;

    public String currentStatus;

}
