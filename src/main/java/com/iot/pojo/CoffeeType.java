package com.iot.pojo;

/**
 * @author isudarik
 * @since 16.03.2017
 */
public enum CoffeeType {
    BIG, SMALL;
}
