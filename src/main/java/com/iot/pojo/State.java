package com.iot.pojo;

/**
 * @author isudarik
 * @since 06.04.2017
 */
public enum State {
    FINISHED, ERROR;
}
