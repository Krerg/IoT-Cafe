package com.iot.pojo.request;

import com.iot.pojo.CoffeeType;

/**
 * @author isudarik
 * @since 16.03.2017
 */
public class CoffeeMakingRequest {
    private CoffeeType coffeeType;

    public CoffeeType getCoffeeType() {
        return coffeeType;
    }

    public void setCoffeeType(CoffeeType coffeeType) {
        this.coffeeType = coffeeType;
    }
}
