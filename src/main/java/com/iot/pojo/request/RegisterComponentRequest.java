package com.iot.pojo.request;

import com.iot.pojo.ClientType;

/**
 * @author isudarik
 * @since 14.03.2017
 */
public class RegisterComponentRequest {
    private String ip;
    private String port;
    private String name;
    private ClientType ty;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public ClientType getTy() {
        return ty;
    }

    public void setTy(ClientType ty) {
        this.ty = ty;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
