package com.iot.pojo.request;

import com.iot.pojo.State;

/**
 * @author isudarik
 * @since 06.04.2017
 */
public class StateChangedRequest {
    private State state;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
