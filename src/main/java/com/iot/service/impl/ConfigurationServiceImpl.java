package com.iot.service.impl;

import com.iot.IoTComponent;
import com.iot.exception.SystemNotConfiguredException;
import com.iot.pojo.ClientType;
import com.iot.service.ConfigurationService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * @author amylnikov
 */
@Service
public class ConfigurationServiceImpl implements ConfigurationService {

    private IoTComponent coffeeMachine;

    private IoTComponent apolloMini;

    @Override
    public boolean isSystemConfigured() {
        return coffeeMachine != null; // && apolloMini != null;
    }

    @Override
    public void addComponent(IoTComponent ioTComponent) {
        Assert.notNull(ioTComponent, "Component to be added should not be null");
        Assert.notNull(ioTComponent.getClientType(), "Client type should be provided");
        Assert.isTrue(!StringUtils.isEmpty(ioTComponent.getIpAddress()), "Component address should not be null");
        Assert.isTrue(!StringUtils.isEmpty(ioTComponent.getOpenedPort()), "Component port should not be null");
        Assert.isTrue(!StringUtils.isEmpty(ioTComponent.getComponentName()), "Component name should not be null");
        switch (ioTComponent.getClientType()) {
        case MACHINE:
            coffeeMachine = ioTComponent;
            break;
        case PLATFORM:
            apolloMini = ioTComponent;
            break;
        default:
            throw new UnsupportedOperationException();
        }
        System.out.print(String.format("%s %s added, info:\nip: %s\nport: %s\n",
                ioTComponent.getClientType(),
                ioTComponent.getComponentName(),
                ioTComponent.getIpAddress(),
                ioTComponent.getOpenedPort()));
    }

    @Override
    public String buildURL(ClientType clientType, String urlPath) throws SystemNotConfiguredException {
        String baseUrl;
        switch (clientType) {
        case MACHINE:
            if (coffeeMachine == null) {
                throw new SystemNotConfiguredException();
            }
            baseUrl = getComponentBaseUrl(coffeeMachine);
            break;
        case PLATFORM:
            if (apolloMini == null) {
                throw new SystemNotConfiguredException();
            }
            baseUrl = getComponentBaseUrl(apolloMini);
            break;

        default:
            throw new UnsupportedOperationException();

        }
        String resultUrl = baseUrl + urlPath;
        return resultUrl;
    }

    private static String getComponentBaseUrl(IoTComponent ioTComponent) {
        return "http://" + ioTComponent.getIpAddress() + ":" + ioTComponent.getOpenedPort();
    }

}
