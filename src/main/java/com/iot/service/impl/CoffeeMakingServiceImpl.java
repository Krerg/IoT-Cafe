package com.iot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.iot.cv.RestCV;
import com.iot.pojo.ClientType;
import com.iot.pojo.CoffeeType;
import com.iot.service.CafeStatus;
import com.iot.service.CoffeeMakingService;
import com.iot.service.ConfigurationService;

/**
 * @author amylnikov
 */
@Service
public class CoffeeMakingServiceImpl implements CoffeeMakingService {

    private int coffeeCount = 0;

    private int distance = 0;

    private int trackLength = 6;

    @Autowired
    private ConfigurationService configurationService;

    private RestTemplate restTemplate = new RestTemplate();

    private CafeStatus currentStatus = CafeStatus.READY;

    private CoffeeType coffeeToPour = CoffeeType.BIG;

    public void increaseCoffeeCount() {
        coffeeCount++;
        distance+=trackLength;
    }

    public int getCoffeeCount() {
        return coffeeCount;
    }

    @Override
    public int getDistance() {
        return distance;
    }

    @Override
    public boolean isReadyToStart() {
        return currentStatus == CafeStatus.READY;
    }

    @Override
    public CafeStatus getCurrentStatus() {
        return currentStatus;
    }

    @Override
    public boolean start(CoffeeType coffeeType) {
        if (currentStatus != CafeStatus.READY) {
            //return false;
        }
        coffeeToPour = coffeeType;
        try {
            ResponseEntity<Void> response = restTemplate
                    .postForEntity(configurationService.buildURL(ClientType.PLATFORM, RestCV.FORWARD_URL_PATH), null, Void.class);
            currentStatus = CafeStatus.WAITING_PLATFORM;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean pourCoffee() {
        String url;
        try {
            switch (coffeeToPour) {
            case BIG:
                url = configurationService.buildURL(ClientType.MACHINE, RestCV.BIG_COFFEE_URL_PATH);
                break;

            case SMALL:
                url = configurationService.buildURL(ClientType.MACHINE, RestCV.SMALL_COFFEE_URL_PATH);
                break;

            default:
                throw new UnsupportedOperationException();
            }
            ResponseEntity<Void> response = restTemplate.postForEntity(url, null, Void.class);
            currentStatus = CafeStatus.POURING_COFFEE;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean returnCoffee() {
        try {
            ResponseEntity<Void> response = restTemplate
                    .postForEntity(configurationService.buildURL(ClientType.PLATFORM, RestCV.BACKWARD_URL_PATH), null, Void.class);
            currentStatus = CafeStatus.RETURNING_COFFEE;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void finishOrder() {
        if (currentStatus != CafeStatus.RETURNING_COFFEE) {
            return;
        }
        currentStatus = CafeStatus.READY;
    }

    @Override
    public void resetState() {
        currentStatus = CafeStatus.READY;
        coffeeToPour = CoffeeType.BIG;
    }

}
