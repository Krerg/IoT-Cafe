package com.iot.service;

import com.iot.pojo.CoffeeType;

/**
 * @author amylnikov
 */
public interface CoffeeMakingService {

    boolean isReadyToStart();

    CafeStatus getCurrentStatus();

    boolean start(CoffeeType coffeeType);

    boolean pourCoffee();

    boolean returnCoffee();

    void finishOrder();

    void resetState();

    void increaseCoffeeCount();

    int getCoffeeCount();

    int getDistance();

}
