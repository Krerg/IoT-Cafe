package com.iot.service;

import com.iot.IoTComponent;
import com.iot.exception.SystemNotConfiguredException;
import com.iot.pojo.ClientType;

/**
 * @author amylnikov
 */
public interface ConfigurationService {

    boolean isSystemConfigured();

    void addComponent(IoTComponent component);

    String buildURL(ClientType clientType, String urlPath) throws SystemNotConfiguredException;

}
