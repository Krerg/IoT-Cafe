package com.iot.service;

/**
 * IoT cafe's status
 */
public enum CafeStatus {

    READY("Ready"), WAITING_PLATFORM("Waiting for platform"), POURING_COFFEE("Pouring coffee"), RETURNING_COFFEE("Returning coffee");
    String status;

    CafeStatus(String status) {
    }
}
