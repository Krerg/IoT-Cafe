package com.iot.rest;

import com.iot.IoTComponent;
import com.iot.pojo.CoffeeType;
import com.iot.pojo.request.CoffeeMakingRequest;
import com.iot.pojo.request.RegisterComponentRequest;
import com.iot.pojo.request.StateChangedRequest;
import com.iot.pojo.request.StatisticsRequest;
import com.iot.pojo.response.RegisterResponse;
import com.iot.pojo.response.SimpleResponse;
import com.iot.pojo.response.StatisticsResponse;
import com.iot.pojo.response.StatusResponse;
import com.iot.service.CafeStatus;
import com.iot.service.CoffeeMakingService;
import com.iot.service.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author amylnikov
 */
@RestController
@RequestMapping(value = "/coffee")
public class CafeMainController {

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private CoffeeMakingService coffeeMakingService;

    @RequestMapping(path = "/status", method = RequestMethod.GET)
    @ResponseBody
    public StatusResponse getStatus() {
        StatusResponse response = new StatusResponse();
        response.setConfigured(configurationService.isSystemConfigured());
        response.setStatus(coffeeMakingService.getCurrentStatus().name());
        return response;
    }

    @RequestMapping(path = "/", method = RequestMethod.POST)
    @ResponseBody
    public SimpleResponse pourCoffee(@RequestBody CoffeeMakingRequest coffeeMakeRequest) {
        SimpleResponse response = new SimpleResponse();
        if (!configurationService.isSystemConfigured()) {
            response.setMessage("System isn't configured");
            response.setResponseStatus("NOK");
            return response;
        }
        if (coffeeMakingService.start(coffeeMakeRequest.getCoffeeType())) {
            response.setMessage("Pouring");
            response.setResponseStatus("OK");
        } else {
            response.setMessage("Not successful!");
            response.setResponseStatus("NOK");
        }
        return response;
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<RegisterResponse> register(@RequestBody RegisterComponentRequest registerRequest) {
        IoTComponent ioTComponent = new IoTComponent();
        ioTComponent.setClientType(registerRequest.getTy());
        ioTComponent.setIpAddress(registerRequest.getIp());
        ioTComponent.setOpenedPort(registerRequest.getPort());
        ioTComponent.setComponentName(registerRequest.getName());
        configurationService.addComponent(ioTComponent);
        RegisterResponse bodyResponse = new RegisterResponse();
        bodyResponse.setMessage("OK");
        return ResponseEntity.ok(bodyResponse);
    }

    @RequestMapping(path = "/state", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<SimpleResponse> changeState(@RequestBody StateChangedRequest stateChangedRequest) {
        SimpleResponse simpleResponse = new SimpleResponse();
        CafeStatus currentStatus = coffeeMakingService.getCurrentStatus();
        boolean isSuccess;
        switch (currentStatus) {
        case WAITING_PLATFORM:
            isSuccess = coffeeMakingService.pourCoffee();
            break;
        case POURING_COFFEE:
            isSuccess = coffeeMakingService.returnCoffee();
            break;
        case RETURNING_COFFEE:
            coffeeMakingService.increaseCoffeeCount();
            coffeeMakingService.finishOrder();
            isSuccess = true;
            break;
        default:
            isSuccess = false;
            break;
        }
        if(isSuccess) {
            simpleResponse.setMessage("OK");
            simpleResponse.setResponseStatus("OK");
        } else {
            simpleResponse.setMessage("System in wrong state: " + currentStatus);
            simpleResponse.setResponseStatus("NOK");

        }
        return ResponseEntity.ok(simpleResponse);
    }

    @RequestMapping(path = "/statistics" , method = RequestMethod.GET)
    @ResponseBody
    public StatisticsResponse getStatistics(@RequestBody StatisticsRequest request) {
        StatisticsResponse response = new StatisticsResponse();
        response.coffeeCount = coffeeMakingService.getCoffeeCount();
        response.distance = coffeeMakingService.getDistance();
        return response;
    }

    @RequestMapping(path = "/reset", method = RequestMethod.POST)
    public void reset() {
        coffeeMakingService.resetState();
    }

}
