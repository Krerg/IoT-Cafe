package com.iot.rest;

import com.iot.pojo.CoffeeType;
import com.iot.pojo.response.SimpleResponse;
import com.iot.service.CoffeeMakingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author amylnikov
 */
@RestController
@RequestMapping(value = "/test")
public class TestController {

    @Autowired
    private CoffeeMakingService coffeeMakingService;

    @RequestMapping(path = "/go", method = RequestMethod.POST)
    @ResponseBody
    public SimpleResponse go() {
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setMessage("OK");
        simpleResponse.setResponseStatus("OK");
        coffeeMakingService.start(CoffeeType.BIG);
        return simpleResponse;
    }

    @RequestMapping(path = "/back", method = RequestMethod.POST)
    @ResponseBody
    public SimpleResponse goBack() {
        SimpleResponse simpleResponse = new SimpleResponse();
        simpleResponse.setMessage("OK");
        simpleResponse.setResponseStatus("OK");
        coffeeMakingService.returnCoffee();
        return simpleResponse;
    }

}
