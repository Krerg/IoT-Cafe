package com.iot.rest;

import com.iot.exception.SystemNotConfiguredException;
import com.iot.pojo.ClientType;
import com.iot.pojo.response.SystemInformationResponse;
import com.iot.service.CoffeeMakingService;
import com.iot.service.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author amylnikov
 */
@RestController
@RequestMapping(value = "/sysinfo")
public class SystemInformationController {

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private CoffeeMakingService coffeeMakingService;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    @ResponseBody
    public SystemInformationResponse getSystemInformation() {
        SystemInformationResponse response = new SystemInformationResponse();
        try {
            response.apolloMiniIp = configurationService.buildURL(ClientType.PLATFORM,"");
        } catch (SystemNotConfiguredException e) {
            response.apolloMiniIp = "Not configured";
        }
        try {
            response.coffeeMachineIp = configurationService.buildURL(ClientType.MACHINE,"");
        } catch (SystemNotConfiguredException e) {
            response.coffeeMachineIp = "Not configured";
        }
        response.currentStatus = coffeeMakingService.getCurrentStatus().toString();
        return response;
    }

}
