package com.iot.cv;

/**
 * @author amylnikov
 */
public class RestCV {

    public static final String FORWARD_URL_PATH = "/platform/move";

    public static final String BACKWARD_URL_PATH = "/platform/back";

    public static final String SMALL_COFFEE_URL_PATH = "/coffee/small";

    public static final String BIG_COFFEE_URL_PATH = "/coffee/big";

}
