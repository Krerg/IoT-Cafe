package com.iot;

import com.iot.pojo.ClientType;

/**
 * @author isudarik
 * @since 20.03.2017
 */
public class IoTComponent {
    private String componentName;

    private String ipAddress;

    private String openedPort;

    private ClientType clientType;

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getOpenedPort() {
        return openedPort;
    }

    public void setOpenedPort(String openedPort) {
        this.openedPort = openedPort;
    }

    public ClientType getClientType() {
        return clientType;
    }

    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }
}
